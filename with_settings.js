import { get } from './'

export default function (destination, defaultSettingsPath) {
    Object.defineProperty(destination, 'settings', {
        get: () => get(destination.settingsPath || defaultSettingsPath),
    })
}
